# DevSecOps Field Manual

TODO: Please Describe the book here.

Author: Dominik Sigmund

## Technical Stuff

The Book is written and created using the [easy book generator](https://gitlab.com/sigmund.dominik/easy-book-generator)

### States of Chapters

1. todo: The chapter needs to be created or has significant work remaining.
2. draft: The chapter has been started but is not yet complete.
3. review: The chapter is complete but needs to be reviewed.
4. done: The chapter is complete and no further work is needed.

```mermaid
stateDiagram-v2
    [*] --> Todo
    Todo --> Draft : Start
    Draft --> Review : Submit for Review
    Review --> Done : Approve
    Review --> Draft : Request Changes
    Done --> [*]
```

- Start at `Todo`
- Move to `Draft`
- Submit for review (`Draft` -> `Review`)
- Approve (`Review` -> `Done`)
- Request changes (`Review` -> `Draft`)
- Finish at `Done`


## Contributing

We appreciate and welcome contributions from the community to enhance the features and overall quality of this book. Whether you're a developer, tester, or enthusiastic user, there are several ways you can contribute:

### Creating Issues

If you encounter a typo, have a chapter request, or want to suggest improvements, please [create an issue](https://gitlab.com/sigmund.dominik/devsecops-field-manual/-/issues/new) on our Gitlab repository. When creating an issue, provide detailed information about the problem or enhancement you're addressing. This includes any relevant context that can help our team understand and address it effectively.

### Merge Requests

If you'd like to contribute text, documentation, or fixes, we encourage you to submit a pull request. Before creating a pull request, please:

1. Fork the repository.
2. Create a new branch for your changes.
3. Make your modification
4. Update this README if necessary
5. Submit a merge request to the `main` branch of the repository.

We'll review your merge request, provide feedback, and work with you to ensure that your contribution aligns with the project's goals and standards.

## License

The DevSecOps Field Manual is licensed under the [MIT License](LICENSE.md). Feel free to use, modify, and distribute it according to your needs.

## Authors

- **[Dominik Sigmund](https://gitlab.com/sigmund.dominik)** - Lead Author and Developer

## Acknowledgments

We appreciate the collaborative efforts that have contributed to the success of the DevSecOps Field Manual. We'd like to thank the following individuals and organizations for their support and contributions:

- **Tom Herfort** - Cover Designer, DevOps Loop Image

## Support

If you can't find a solution to your issue in the documentation, feel free to reach out to us for assistance. We offer support through the following channels:

- **Email:** For non-urgent matters or if you prefer email communication, you can reach us at [dsofm@webdad.eu](mailto:dsofm@webdad.eu). Please provide detailed information about your issue so that we can assist you more effectively.

## Important Note

When seeking support, make sure to include the following information in your message:

1. A detailed description of the issue you're facing.
2. Steps to reproduce the problem (if applicable).
3. Relevant configuration details.
4. Any error messages or logs related to the issue.

This information will help us understand your situation better and provide a more accurate and timely response.

Thank you for choosing DSOFM!! We're committed to ensuring you have a positive experience, and we appreciate your cooperation in following these support guidelines.
