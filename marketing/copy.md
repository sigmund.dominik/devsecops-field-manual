---
tag: draft
---

# Copy

Welcome to "The DevSecOps Fieldmanual" - the ultimate guide for mastering the art of DevOps with a special focus on security!

![DSOFM Logo (c) Thomas Herfort](media/live/logo.png "DSOFM Logo (c) Thomas Herfort"){.center loading="lazy" width="410" height="371"}

Inspired by "The Red Team FieldManual" and "The Blue Team FieldManual," this book is designed to provide comprehensive guidance on the concepts behind DevOps, as well as a wide range of cheat sheets for popular tools used in the industry.

While security is not the primary focus of this book, it is an essential aspect of DevOps that cannot be overlooked. Therefore, we have included a dedicated chapter on DevSecOps to help you understand how to integrate security into your DevOps workflow seamlessly.

Whether you are a seasoned DevOps professional or just starting, this book is your go-to resource for learning new skills and honing your existing ones. Each chapter is designed to be concise and practical, providing you with the knowledge and tools you need to excel in your job.

From setting up a DevOps environment to optimizing your toolchain, this book covers it all. We've included cheat sheets for popular tools such as Jenkins, Docker, Kubernetes, and more, so you can quickly reference them whenever you need.

We are confident that "The DevSecOps Fieldmanual" will be an indispensable resource for you and your team. Whether you are a developer, operations professional, or security engineer, this book will provide you with the knowledge and skills you need to succeed in today's fast-paced world of DevOps.

Thank you for choosing "The DevSecOps Fieldmanual," and we look forward to hearing about your successes in the field of DevOps!
