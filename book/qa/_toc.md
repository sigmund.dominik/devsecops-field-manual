---
tag: done
---

# QA

\newpage

!include book/qa/automated-testing-strategies.md

\newpage

!include book/qa/continuous-integration-and-continuous-testing.md

\newpage

!include book/qa/implementing-shift-left-testing.md

\newpage

!include book/qa/test-case-prioritization.md

\newpage

!include book/qa/quality-gates.md

\newpage

!include book/qa/performance-testing-in-continuous-delivery.md

\newpage

!include book/qa/load-and-stress-testing.md

\newpage

!include book/qa/security-testing-in-the-devops-pipeline.md

\newpage

!include book/qa/service-virtualization.md

\newpage

!include book/qa/mobile-and-cross-platform-testing-strategies.md

\newpage

!include book/qa/qa-environment-management.md

\newpage

!include book/qa/test-data-management.md

\newpage

!include book/qa/monitoring-and-feedback-for-quality-assurance.md

\newpage

!include book/qa/user-experience-testing-in-devops.md

\newpage

!include book/qa/accessibility-testing-for-inclusive-software.md

\newpage

!include book/qa/risk-based-testing-in-devops.md

\newpage

!include book/qa/leveraging-ai-and-machine-learning-in-qa.md

\newpage

!include book/qa/flaky-tests-management.md

\newpage

!include book/qa/devops-testing-metrics-and-kpis.md

\newpage

!include book/qa/exploratory-testing-in-agile-teams.md

\newpage

!include book/qa/continuous-feedback-and-improvement-in-qa.md
