---
tag: done
---

# Security

\newpage

!include book/security/security-as-code.md

\newpage

!include book/security/security-policy-as-code.md

\newpage

!include book/security/thread-modeling.md

\newpage

!include book/security/static-application-security-testing.md

\newpage

!include book/security/dynamic-application-security-testing.md

\newpage

!include book/security/interactive-application-security-testing.md

\newpage

!include book/security/dependency-scanning.md

\newpage

!include book/security/container-security.md

\newpage

!include book/security/encryption-practices.md

\newpage

!include book/security/vulnerability-management.md

\newpage

!include book/security/penetration-testing.md

\newpage

!include book/security/access-control-and-least-privilege.md

\newpage

!include book/security/identity-and-access-management.md

\newpage

!include book/security/secrets-management.md

\newpage

!include book/security/zero-trust.md

\newpage

!include book/security/continuous-monitoring-and-threat-intelligence.md

\newpage

!include book/security/audit-trails-and-logging.md

\newpage

!include book/security/compliance-and-regulatory-requirements.md

\newpage

!include book/security/security-in-the-cicd-pipeline.md

\newpage

!include book/security/security-automation.md

\newpage

!include book/security/security-incident-response.md

\newpage

!include book/security/risk-assessment-and-management.md

\newpage

!include book/security/security-training-and-awareness.md

\newpage

!include book/security/metrics-and-key-performance-indicators-for-security.md
