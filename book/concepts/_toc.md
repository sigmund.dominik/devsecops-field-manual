---
tag: done
---

# Concepts

\newpage

!include book/concepts/101-basics.md

\newpage

!include book/concepts/calms.md

\newpage

!include book/concepts/the-3-ways.md

\newpage

!include book/concepts/4-types-of-work.md

\newpage

!include book/concepts/5-ideals-of-development.md

\newpage

!include book/concepts/6-capabilities-of-devsecops.md

\newpage

!include book/concepts/7-types-of-waste.md

\newpage

!include book/concepts/the-andon-cord.md

\newpage

!include book/concepts/kaizen.md

\newpage

!include book/concepts/top-10-reasons-for-devsecops.md

\newpage

!include book/concepts/top-11-pitfalls.md

\newpage

!include book/concepts/12-factor-app.md

\newpage

!include book/concepts/the-loop.md

\newpage

!include book/concepts/code-review.md

\newpage

!include book/concepts/version-control-systems.md

\newpage

!include book/concepts/configuration-management.md

\newpage

!include book/concepts/continous-integration.md

\newpage

!include book/concepts/continous-delivery.md

\newpage

!include book/concepts/test-automation.md

\newpage

!include book/concepts/infrastructure-as-code.md

\newpage

!include book/concepts/microservices.md

\newpage

!include book/concepts/containerization.md

\newpage

!include book/concepts/kubernetes-and-container-orchestration.md

\newpage

!include book/concepts/serverless-architecture.md

\newpage

!include book/concepts/monitoring-and-observability.md

\newpage

!include book/concepts/event-driven-architecture.md

\newpage

!include book/concepts/compliance-and-governance.md

\newpage

!include book/concepts/dora.md
