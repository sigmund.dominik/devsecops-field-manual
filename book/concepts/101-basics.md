---
tag: done
created: 2024-06-30
author: Dominik Sigmund
---

# 101 - Basics

DevOps is a set of practices that combines software development (Dev) and IT operations (Ops). It aims to shorten the systems development life cycle and provide continuous delivery with high software quality. DevOps is complementary with Agile software development; several DevOps aspects came from the Agile methodology

DevSecOps is an augmentation of DevOps to allow for security practices to be integrated into the DevOps approach. Contrary to a traditional centralized security team model, each delivery team is empowered to factor in the correct security controls into their software delivery. Security practices and testing are performed earlier in the development lifecycle, hence the term "shift left" can be used. Security is tested in three main areas: static, software composition, and dynamic.

## Short History

In 2009, the first conference called devopsdays was held in Ghent, Belgium. The conference was founded by Belgian consultant, project manager and agile practitioner Patrick Debois. The conference has since spread to other countries.

In 2012, the State of DevOps report was conceived and launched by Alanna Brown at Puppet.

Since 2014, the annual State of DevOps report has been published by Nicole Forsgren, Gene Kim, Jez Humble and others. They found that the adoption of DevOps was accelerating. Also in 2014, Lisa Crispin and Janet Gregory wrote the book More Agile Testing, which included a chapter on testing and DevOps.

2016's State of DevOps report included DORA metrics for velocity (deployments, lead time to change) and stability (mean time to restore, change failure rate).

In 2018, the book Accelerate: The Science of Lean Software and DevOps" by Nicole Forsgren, Jez Humble, and Gene Kim was published, providing scientific data on the benefits of DevOps practices and further popularizing the DORA metrics.

2019, the DevOps Institute released the Upskilling: Enterprise DevOps Skills Report," highlighting the critical skills and competencies needed for DevOps professionals.

The COVID-19 pandemic in 2020 accelerated the adoption of DevOps practices as organizations worldwide transitioned to remote work and sought to increase their agility and resilience.

In 2021, the State of DevOps Report continued to show the increasing maturity and evolution of DevOps practices, highlighting the importance of security (DevSecOps) and cultural change within organizations.

In 2022, GitHub introduced GitHub Copilot, an AI-powered code completion tool that has had a significant impact on the way DevOps and development teams work, highlighting the increasing integration of AI into DevOps practices.

Since 2023, the DevOps community saw a rise in the adoption of GitOps, an operational framework that uses Git repositories as the single source of truth for infrastructure and application delivery, further simplifying and securing the DevOps pipeline.

## Sources / Further Reading

* [The Phoenix Project: A Novel about IT, DevOps, and Helping Your Business Win](https://www.amazon.de/-/en/Gene-Kim-ebook/dp/B09JWVXFNG)
* [The Unicorn Project: A Novel about Developers, Digital Disruption, and Thriving in the Age of Data](https://www.amazon.de/-/en/gp/product/B07QT9QR41)
* [The DevOps Handbook: How to Create World-Class Agility, Reliability, &amp; Security in Technology Organizations](https://www.amazon.de/-/en/Gene-Kim-ebook/dp/B09G2GS39R)
* [Accelerate: The Science of Lean Software and DevOps: Building and Scaling High Performing Technology Organizations](https://www.amazon.de/-/en/Nicole-Forsgren-PhD-ebook/dp/B07B9F83WM)
* [Sooner Safer Happier: Antipatterns and Patterns for Business Agility](https://www.amazon.de/-/en/Jonathan-Smart-ebook/dp/B0889L7Y9D)
* [DevOps for Dummies](https://www.amazon.de/-/en/Freeman/dp/1119552222)
