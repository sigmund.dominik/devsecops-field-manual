---
tag: done
created: 2024-06-30
author: Dominik Sigmund
---

# The Enhanced Loop

![Start &gt; Plan &gt; Code &gt; Build &gt; Test &gt; Release &gt; Deploy &gt; Operate &gt; Monitor &gt; End](images//devops-workflow.png "Improved DevOps Worflow by Tom Herfort"){.center loading="lazy" width="100%" height="auto"}
*Improved DevOps Workflow by Tom Herfort*

We improved on the original devops pipeline, giving it a meaningful start and end

## Start

- **Requirement**: We need to collect requirements and use-cases to create meaningful services.
- **Concept**: A clear concept on what we will build and what the value is will be created as starting point
- **Project**: People and Resources need to be allocated, as well as internal requirements need to be observed
- **Commites**: More often the Less, applications need to pass commites (work councils, privacy, etc). By shifting this left, we can get approval before starting the work, minimizing the waiting time here.

## The Loop

- **Plan**: Collect Use Cases from Stakeholders, Priorize them, Manage the backlog, decide what to do in the next sprint
- **Code**: Create Code and Tests. This step normally ends with a code review and a merge into production.
- **Build**: Using a CI / CD Pipeline, deployable artifacts will be created
- **Test**: Automatic Tests, as well as SAST and DAST run here. Finally acceptance tests will be run to allow for release into production
- **Release**: Create a finalized version for production
- **Deploy**: With zero-downtime, replace the running release with the new artifact
- **Operate**: The release will run and be managed
- **Monitor**: Measure Metrics and use the understanding to create new user-stories

## End of Life

If the service is no longer needed or is superseded by another service, it must be deactivated.
