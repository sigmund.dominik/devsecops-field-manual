---
tag: done
created: 2024-06-30
author: Dominik Sigmund
---

# The 12 Factors for any App

The 12-factor app is a methodology for building software-as-a-service apps that, among other things, are scalable, maintainable, and resilient. The methodology was created by developers at Heroku, a platform-as-a-service provider. The 12 factors are a set of best practices that help developers build apps that are easy to deploy and scale in the cloud.

- **I. Codebase**: One codebase tracked in revision control, many deploys
- **II. Dependencies**: Explicitly declare and isolate dependencies
- **III. Config**: Store config in the environment
- **IV. Backing services**: Treat backing services as attached resources
- **V. Build, release, run**: Strictly separate build and run stages
- **VI. Processes**: Execute the app as one or more stateless processes
- **VII. Port binding**: Export services via port binding
- **VIII. Concurrency**: Scale out via the process model
- **IX. Disposability**: Maximize robustness with fast startup and graceful shutdown
- **X. Dev/prod parity**: Keep development, staging, and production as similar as possible
- **XI. Logs**: Treat logs as event streams
- **XII. Admin processes**: Run admin/management tasks as one-off processes
