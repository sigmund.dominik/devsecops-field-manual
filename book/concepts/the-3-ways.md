---
tag: done
created: 2024-06-30
author: Dominik Sigmund
---

# The 3 Ways

DevOps is a set of practices that combines software development (Dev) and IT operations (Ops) to shorten the development life cycle and provide continuous delivery with high software quality. The 3 Ways of DevOps are principles that guide these practices, focusing on improving the flow of work, feedback, and continuous learning and experimentation.

```mermaid
graph TD;
    A[First Way: System Thinking] --> |Enables| B[Second Way: Feedback Loops ]
    B --> |Promotes| C[Third Way: Culture of Continual Experimentation and Learning ]
    C --> |Improves| A
```

## 1. The First Way: Flow

The First Way emphasizes the performance of the entire system and increases the flow of work from Development to Operations to the customer. Key practices include:

- **Automating the Build and Deployment Pipeline**: Automate everything that can be automated to reduce lead times and errors.
- **Version Control for All Production Artifacts**: Ensure that every part of the system can be reproduced based on version-controlled sources.
- **Reducing Batch Sizes and Intervals of Work**: Smaller changes are easier to deploy and troubleshoot.

## 2. The Second Way: Feedback

The Second Way focuses on creating a fast and effective feedback loop from Operations back to Development. Key practices include:

- **Implementing Feedback Loops**: Ensure that feedback from monitoring and production incidents are quickly and effectively communicated back to Development.
- **Swarming to Solve Problems When They Occur**: Encourage teams to collaborate intensively to resolve critical issues, leading to faster resolution and learning.
- **Incorporating Learning from Failures**: Use failures as an opportunity to improve the system and the team's response capabilities.

## 3. The Third Way: Continual Learning and Experimentation

The Third Way encourages a culture of continuous experimentation, taking risks, and learning from failure. Key practices include:

- **Fostering a Culture of Experimentation and Learning**: Encourage taking risks and learning from failure without fear of negative consequences.
- **Allocating Time for Improvement**: Dedicate time for process improvement and experimentation to foster innovation.
- **Encouraging Learning from All Sources**: Learn from other disciplines and integrate that knowledge into DevOps practices.

These three ways are interdependent, with each one supporting and enhancing the others. Implementing the 3 Ways of DevOps requires a cultural shift in the organization, emphasizing collaboration, communication, and continuous improvement.
