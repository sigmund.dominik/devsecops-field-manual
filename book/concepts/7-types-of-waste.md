---
tag: done
created: 2024-06-30
author: Dominik Sigmund
---

# 7 Types of Waste

## Overproduction

Creating more code, features and functions than the customer needs at the time. Anything we build that the customer is not ready to “consume” is considered over production.

## Over-processing

Creating features beyond what the customer has requested, in other words, doing more work than is required. This is a cousin to overproduction but focuses on excess functionality as contrasted to excess product.

## Defects

Defects are, well, defects: any work product that is not 100% correct and complete contains defects.

## Over-engineering

Making things more complicated that necessary.  This is also related to over production but focuses on excess complexity versus excess production.

## Inventory

In a factory, inventory is all the work in process (WIP) and finished goods sitting on the shelf waiting for customer orders. In DevOps inventory is all the partially done work. An example of WIP inventory is the backlog of open tickets.

## Transportation

Transportation refers to movement of product. In DevOps, this includes movement of code as well as endless emails and IM’s which seem to linger on for far too long. Excessive review and approval processes fall into this category as well.

## Waiting

This waste addresses all the waiting that occurs by both people and product. If a person is waiting for anything (code, information, etc.) before they can do their job, that wait time is wasteful.
