---
tag: done
created: 2024-06-30
author: Dominik Sigmund
---

# The Andon Cord

The Andon Cord is a key concept from lean manufacturing, symbolized by a physical rope that, when pulled, would instantly stop all work on the assembly line. This practice empowers anyone to pull the cord at any time if they discover a problem.

When the cord is pulled, all production is halted immediately. A team leader must then quickly approach the person who pulled the cord to understand the issue. Together, the leader and the team work to solve the problem and restart production.

The Andon Cord is an abrupt yet effective way of addressing manufacturing issues at the source, preventing small problems from escalating into major defects. This practice not only emphasizes the importance of quality and problem-solving but also fosters a culture of continuous improvement and collective responsibility.

By encouraging immediate attention and resolution, the Andon Cord helps maintain high standards of quality and efficiency, ensuring that issues are addressed promptly and do not recur.

This expanded text maintains the brevity and precision of the original while providing additional context and emphasizing the importance of the Andon Cord in promoting quality and teamwork.
