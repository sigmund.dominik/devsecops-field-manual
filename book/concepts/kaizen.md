---
tag: done
created: 2024-06-30
author: Dominik Sigmund
---

# Kaizen

Kaizen is an approach to creating continuous improvement based on the idea that small, ongoing positive changes can reap significant improvements.

## 10 principles of Kaizen

Because executing Kaizen requires enabling the right mindset throughout a company, 10 principles that address the Kaizen mindset are commonly referenced as core to the philosophy. They are:

* Let go of assumptions.
* Be proactive about solving problems.
* Don't accept the status quo.
* Let go of perfectionism and take an attitude of iterative, adaptive change.
* Look for solutions as you find mistakes.
* Create an environment in which everyone feels empowered to contribute.
* Don't accept the obvious issue; instead, ask "why" five times to get to the root cause.
* Cull information and opinions from multiple people.
* Use creativity to find low-cost, small improvements.
* Never stop improving.

## Kaizen cycle for continuous improvement

Kaizen can be implemented in a seven-step cycle to create an environment based on continuous improvement. This systematic method includes the following steps:

1. Get employees involved.
2. Find problems.
3. Create a solution.
4. Test the solution.
5. Analyze the results.
6. If results are positive, adopt the solution throughout the organization.
7. Repeat

## Kaizen Blitz

A Kaizen Blitz is a rapid, intensive activity or event that attacks a specific problem with overwhelming resources to rapidly come to a solution.
It is the organized use of team knowledge that can improve all aspects of your organization. Such an event assembles cross-functional teams aimed at improving a process or problem identified within a specific area. You can use a Kaizen Blitz in the following situations:

* When obvious problems have been identified but no known solutions exist.
* When the scope and boundaries of a problem are clearly defined and relatively narrow in scope.
* When results are needed in the short term.
* In the early stages of a project to gain momentum and build credibility.
