---
tag: done
created: 2024-06-30
author: Dominik Sigmund
---

# The Top Eleven Pitfalls of DevOps

Fostering a DevOps culture and selecting tools to support your DevOps approach will benefit your organization. However, any time you attempt to make a massive change to the undercurrent of your organization, you face challenges and have to deal with setbacks. Here are some common Pitfalls to deal with.

## Deprioritizing Culture

More than anything else, DevOps is a cultural movement. The culture you build at your organization will make or break your DevOps practice. Your DevOps culture must emphasize collaboration, trust, and engineering empowerment. If you nail automation but miss those cultural components, you will likely fail.

## Leaving Others Behind

Making the case internally for DevOps will determine the type of foundation you build for your culture. Look for fertile soil. If you move too quickly and don’t convince key people of the importance of a DevOps transformation, people will watch your movements with skepticism and leap at the first opportunity to show everyone you’re wrong.

## Forgetting to Align Incentives

If you don’t set out to align incentives with what you expect from certain teams or specific engineers, more challenges arise. The real tool of DevOps, if you can master it, is empowerment. You want to empower your engineers to do their job well, free from interference.

## Keeping Quiet

DevOps is the antithesis of secrets and backroom negotiations. Instead, it lays everything out on the table and forces you to trust the integrity of the people in your organization. When you first introduce open communication, conflict may seem to increase. It doesn’t. Instead, you’re simply seeing the friction points for the first time. Instead of leaving conflict to brew beneath the surface, people feel safe enough to raise their concerns and express their opinions.

## Forgetting to Measure

Measuring your progress is crucial to DevOps success. It lends you validation when making the argument for DevOps to doubting stakeholders, helps you convince holdout executives, and reminds your engineering team how much they’ve accomplished.

## Micromanaging

One of the quickest ways to undermine your engineers is to micromanage their work.

## Changing Too Much, Too Fast

Many teams make too many changes too quickly. Humans don’t like change. Although DevOps is beneficial over the long term, quick changes to the normal way of doing things can be jarring to engineers.

## Choosing Tools Poorly

Although tooling is deprioritized in DevOps it is still a factor. Even the least important aspect of DevOps contributes to your overall success. The tools you select should solve the problems your engineering team experiences, but should also align with the style, knowledge, and comfort areas of your existing team.

## Fearing Failure

Failing fast is a short way of saying you should constantly be iterating to identify problems early in the process without spending a ton of time and money.

## Being Too Rigid

DevOps is not prescriptive, and that’s both the best and worst thing about it.

## Starting from the Top

DevOps should start from the engineers and bubble to the top to allow everyone to help achieve best results.
