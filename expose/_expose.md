---
tag: todo
---

# Expose

## Overview

!include expose/overview.md

\newpage

## Key Features

!include expose/key_features.md

\newpage

## Target Audience

!include expose/target_audience.md

\newpage

## Selling Points

!include expose/selling_points.md

\newpage

## Similar Books

!include expose/similar_books.md

\newpage

## About the Author

!include book/about_the_author.md

\newpage

## Text Sample

TODO: add a text sample